export default {
  data () {
    return {
      // root: 'https://34.213.223.104:3000/',
      // root: 'http://localhost:3000/',
      root: 'https://bi.iideas.biz:3000/',
      curDate: new Date().toISOString().substr(0, 10),
    }
  },
  methods: {
    formatPrice (value) {
        let val = (value / 1).toFixed(2).replace('.', ',')
        return '$ ' + val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
    },

    formatPercentage (value) {
        let val = (value / 1).toFixed(2).replace('.', ',')
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ' %'
    },

    normalize (val, max, min) {
      return ((val - min) / (max - min))
    },

    logNormalize (enteredValue, maxEntry, minEntry, normalizedMax, normalizedMin) {
      var mx = (Math.log1p((enteredValue - minEntry)) / (Math.log(maxEntry - minEntry)))
      var preshiftNormalized = mx * (normalizedMax - normalizedMin)
      var shiftedNormalized = preshiftNormalized + normalizedMin
      return shiftedNormalized
    },
  },
}
