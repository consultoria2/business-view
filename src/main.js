import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins'
import vuetify from './plugins/vuetify'
import VueExcelXlsx from 'vue-excel-xlsx'
import { sync } from 'vuex-router-sync'
import './registerServiceWorker'
import * as VueGoogleMaps from 'vue2-google-maps'
import VueSession from 'vue-session'

// Import the Auth0 configuration
import { domain, clientId } from '../auth_config.json'

// Import the plugin here
import { Auth0Plugin } from './auth'

// Install the authentication plugin here
Vue.use(Auth0Plugin, {
  domain,
  clientId,
  onRedirectCallback: appState => {
    router.push(
      appState && appState.targetUrl
        ? appState.targetUrl
        : window.location.pathname
    );
  }
});

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyA4BsxbxegKTrDSrhcFDaAMoaQECHJdRm8',
    libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
 
    // If you want to set the version, you can do so:
    // v: '3.26',
  },
 
  // If you intend to programmatically custom event listener code
  // (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
  // instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
  // you might need to turn this on.
  // autobindAllEvents: false,
 
  // If you want to manually install components, e.g.
  // import {GmapMarker} from 'vue2-google-maps/src/components/marker'
  // Vue.component('GmapMarker', GmapMarker)
  // then disable the following:
  // installComponents: true,
});

Vue.use(VueSession);

sync(store, router)

Vue.config.productionTip = false

Vue.use(VueExcelXlsx)

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app')
