/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
export default [
  {
    path: '',
    view: 'Dashboard',
    meta: {
      title: 'Business View by Innovart J2',
      metaTags: [
        {
          name: 'description',
          content: 'dashboard inteligencia de negocios',
        },
        {
          property: 'og:description',
          content: 'dashboard inteligencia de negocios',
        },
      ],
    },
  },
  {
    path: '/customers',
    name: 'Clientes',
    view: 'Customers',
  },
  {
    path: '/user-profile',
    name: 'Perfil',
    view: 'UserProfile',
  },
  {
    path: '/table-list',
    name: 'Table List',
    view: 'TableList',
  },
  {
    path: '/typography',
    view: 'Typography',
  },
  {
    path: '/icons',
    view: 'Icons',
  },
  {
    path: '/maps',
    view: 'Maps',
  },
  {
    path: '/notifications',
    view: 'Notifications',
  },
  {
    path: '/upgrade',
    name: 'Upgrade to PRO',
    view: 'Upgrade',
  },
  {
    path: '/sales-details',
    name: 'Sales Details',
    view: 'SalesDetails',
  },
]
